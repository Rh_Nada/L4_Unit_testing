// src/main.js
const express = require("express");
const mylib = require("./mylib");
const app = express();
const port = 3000;

app.get('/', (req, rest) => {
    rest.send('Hello world');
});

app.get('/add', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    const sum = mylib.add(a, b); // calling the function from other file
    res.send(sum.toString());
});

app.get('/substract', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    const substract = mylib.sub(a, b); // calling the function from other file
    res.send(substract.toString());
})

app.listen(port, () => {
    console.log(`Server: http://localhost:${port}`)
});