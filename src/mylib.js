/**
 * This arrow function returns the sum of two parameters.
 * @param {number} param1 this is the first parameter
 * @param {number} param2 this is the second parameter
 * @returns {numbers}
 */

const add = (param1, param2) => {
    const result = param1 + param2;
    return result 
}

/**
 * This is arroaw function too. It is on single line, it has return statement
 * Without curly brackets it doesn't require return keyword.
 * @param {number} a first parameter
 * @param {number} b second parameter
 * @returns 
 */
const substract = (a, b) => {
    const result = a - b;
    return result
}

module.exports = {
    add,
    sub: substract,
    random: () => Math.random(),
    arrayGen: () => [1,2,3]
}