// example.test.js
const expect = require('chai').expect;
const mylib = require('../src/mylib');
const assert = require('chai').expect;
var should = require('should');

describe('Unit testing mylib.js', () => {
    // run tasks before tests in this block
    before(() => {
    myvar =1;
    console.log('Before testing');
        });


    it("Should return 2 when using sum function with parameters a=1, b=1", () => {
        const result = mylib.add(1, 1); 
        expect(result).to.equal(2); 
    })

    it("Should return 12 when using sum function with parameters a=20, b=8", () => {
        const result = mylib.sub(20, 8); 
        expect(result).to.equal(12); 
    })



it('Assert foo is not bar', () => {
    assert('foo' !== 'bar');
})

it('Myvar should exist', () => {
    should.exist(myvar)
})

// run after tests in this block
after(() => console.log('After my testing'))
});


